package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

type numPagesResponse struct {
	Pages    int `json:"pages"`
	PageSize int `json:"pageSize"`
	Blocks   int `json:"blocks"`
}

const (
	indexName = "CC-MAIN-2024-18"
)

func getIndexBaseUrl(query string) string {
	return fmt.Sprintf(
		"http://index.commoncrawl.org/%s-index?url=%s&output=json",
		indexName,
		url.QueryEscape(query),
	)
}

func getNumPages(query string) (int, error) {
	resp, err := http.Get(getIndexBaseUrl(query) + "&showNumPages=true")
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}

	var pageResp numPagesResponse
	if err := json.Unmarshal(body, &pageResp); err != nil {
		return 0, err
	}
	return pageResp.Pages, nil
}

func listRecords(query string, limit int) ([]CommonCrawlRecord, error) {
	nPages, err := getNumPages(query)
	if err != nil {
		return []CommonCrawlRecord{}, err
	}
	records := make(map[string]CommonCrawlRecord)
	end := false
	for page := 0; page < nPages && !end && limit > 0; page++ {
		resp, err := http.Get(getIndexBaseUrl(query) + fmt.Sprintf("&page=%d", page))
		if err != nil {
			return nil, err
		}
		if resp.StatusCode == http.StatusNoContent {
			break
		}
		defer resp.Body.Close()

		scanner := bufio.NewScanner(resp.Body)
		for scanner.Scan() {
			if limit <= 0 {
				break
			}
			var result CommonCrawlRecord
			err := json.Unmarshal(scanner.Bytes(), &result)
			if err != nil {
				end = true
				break
			}
			if result.URLKey == nil {
				end = true
				break
			}
			if info, exists := records[*result.URLKey]; !exists || info.Timestamp < result.Timestamp {
				if !exists {
					limit--
				}
				records[*result.URLKey] = result
			}
		}

		if err := scanner.Err(); err != nil {
			return nil, err
		}
	}
	results := make([]CommonCrawlRecord, len(records))
	i := 0
	for _, record := range records {
		results[i] = record
		i++
	}
	return results, nil
}
