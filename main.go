package main

import (
	"fmt"
	"github.com/spf13/cobra"
	"log"
	"os"
)

type CommonCrawlRecord struct {
	URLKey    *string `json:"urlkey"`
	URL       string  `json:"url"`
	Status    string  `json:"status"`
	Length    string  `json:"length"`
	Offset    string  `json:"offset"`
	Digest    string  `json:"digest"`
	Timestamp string  `json:"timestamp"`
	MIME      string  `json:"mime"`
	Filename  string  `json:"filename"`
}

func main() {
	var limit int
	rootCmd := &cobra.Command{Use: "ccrawl"}
	lsCmd := &cobra.Command{
		Use:   "ls <query>...",
		Short: "List URLs",
		Long:  "List URLs for a particular query: example.com/*",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 0 {
				return fmt.Errorf("Must specify a query")
			}
			return nil
		},
		Run: func(c *cobra.Command, args []string) {
			for _, arg := range args {
				records, err := listRecords(arg, limit)
				if err != nil {
					log.Fatalf("Error: %s", err)
				}
				for _, info := range records {
					fmt.Printf("URL: %s - Status: %s - Length: %s - MIME: %s\n", info.URL, info.Status, info.Length, info.MIME)
				}
			}
		},
	}
	lsCmd.Flags().IntVarP(&limit, "limit", "l", 100, "Max number of URLs")
	getCmd := &cobra.Command{
		Use:   "get <url>",
		Short: "Get content at a particular URL",
		Long:  "Get content at a particular URL",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 0 {
				return fmt.Errorf("Must specify a URL")
			}
			return nil
		},
		Run: func(c *cobra.Command, args []string) {
			records, err := listRecords(args[0], 1)
			if err != nil {
				log.Fatalf("Error: %s", err)
			}
			if len(records) == 0 {
				log.Fatalf("No record found.")
			}
			for _, info := range records {
				if info.Status == "200" {
					if content, err := fetchRecordContent(&info); err == nil {
						fmt.Println(string(content))
						return
					}
				}
			}
		},
	}
	rootCmd.AddCommand(
		lsCmd,
		getCmd,
	)
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
