# ccrawl

`ccrawl` is a command-line tool for querying the Common Crawl dataset. It provides two main commands: one for listing URLs that match a certain query and another for retrieving the content of a specific URL.

## Installation

To install `ccrawl`, clone the repository and build the tool using Go:

```bash
git clone https://github.com/yourusername/ccrawl.git
cd ccrawl
make
```

## Usage

The `ccrawl` tool has two primary commands: `ls` and `get`.

### List URLs (`ls`)

The `ls` command lists URLs from the Common Crawl dataset that match a specific query pattern.

#### Usage

```bash
ccrawl ls <query>... [flags]
```

#### Flags

- `-h, --help`: Display help information for the `ls` command.
- `-l, --limit int`: Set the maximum number of URLs to return (default is 100).

#### Example

To list URLs that match the pattern `example.com/foo/*`:

```bash
ccrawl ls example.com/foo/*
```

### Get Content (`get`)

The `get` command retrieves the content of a specific URL from the Common Crawl dataset.

#### Usage

```bash
ccrawl get <url> [flags]
```

#### Flags

- `-h, --help`: Display help information for the `get` command.

#### Example

To get the content of `example.com/foo/bar.html`:

```bash
ccrawl get example.com/foo/bar.html
```

## Help

For more detailed information about the available commands and flags, you can use the `help` command:

```bash
ccrawl help
```

## License

This project is licensed under the MIT License.

