package main

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"io"
	"strconv"
)

func fetchRecordContent(record *CommonCrawlRecord) ([]byte, error) {
	offset, err := strconv.ParseInt(record.Offset, 10, 64)
	if err != nil {
		return nil, fmt.Errorf("invalid offset: %v", err)
	}
	length, err := strconv.ParseInt(record.Length, 10, 64)
	if err != nil {
		return nil, fmt.Errorf("invalid length: %v", err)
	}

	sess := session.Must(session.NewSession(&aws.Config{
		// Region where Common Crawl data is stored
		Region: aws.String("us-east-1"),
	}))
	svc := s3.New(sess)
	req, resp := svc.GetObjectRequest(&s3.GetObjectInput{
		Bucket: aws.String("commoncrawl"),
		Key:    aws.String(record.Filename),
		Range:  aws.String(fmt.Sprintf("bytes=%d-%d", offset, offset+length-1)),
	})

	err = req.Send()
	if err != nil {
		return nil, fmt.Errorf("failed to fetch WARC file: %v", err)
	}
	defer resp.Body.Close()

	gzipReader, err := gzip.NewReader(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to create gzip reader: %v", err)
	}
	defer gzipReader.Close()

	var buf bytes.Buffer
	if _, err := io.Copy(&buf, gzipReader); err != nil {
		return nil, fmt.Errorf("failed to read WARC content: %v", err)
	}

	content := buf.Bytes()
	// Remove the 2 header sections (WARC + HTTP)
	for i := 0; i < 2; i++ {
		splitIndex := bytes.Index(content, []byte("\r\n\r\n"))
		if splitIndex == -1 {
			return nil, fmt.Errorf("failed to find end of headers")
		}
		content = content[splitIndex+4:]
	}
	return content, nil
}
